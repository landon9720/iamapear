import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { firebaseConnect, pathToJS } from 'react-redux-firebase'
import classes from './UserDisplay.scss'
import _ from 'lodash'
import { Link } from 'react-router'
import Avatar from 'material-ui/Avatar'

@firebaseConnect()
@connect(
  ({ firebase }) => ({
    // propKey: pathToJS(firebase, 'firebasepath')
  })
)
export default class UserDisplay extends Component {
  static propTypes = {
    firebase: PropTypes.object,
    user: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ])
  }

  componentDidMount () {
    const user = this.props.user
    if (_.isString(user)) {
      this.ref = this.props.firebase.ref(`users/${user}`)
      this.ref.on('value', u => this.setState({ asyncUser: _.extend({ id: user }, u.val()) }))
    }
  }

  componentWillUnmount () {
    if (this.ref) {
      this.ref.off()
    }
  }

  render () {
    let user = (this.state && this.state.asyncUser) || (this.props && _.isObject(this.props.user) && this.props.user)
    if (!user) {
      return <div>NO USER</div>
    }
    return <Link to={`/user/${user.id}`}>
      <Avatar src={user.avatarUrl} title={user.displayName} />
    </Link>
  }
}
