import React, { Component, PropTypes } from 'react'
import classes from './Node.scss'
import UserDisplay from '../UserDisplay/UserDisplay'
import _ from 'lodash'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { Link } from 'react-router'
import humanize from 'humanize'

@firebaseConnect(
)
@connect(
  ({ firebase }) => ({
  })
)
export default class Node extends Component {

  static propTypes = {
    auth: PropTypes.object,
    firebase: PropTypes.object,
    node: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string
    ]).isRequired
  }

  componentDidMount () {
    this.load(this.props.node)
  }

  componentWillReceiveProps (nextProps) {
    if (!_.isEqual(this.props.node, nextProps.node)) {
      this.load(nextProps.node)
    }
  }

  load (node) {
    if (this.ref) {
      this.ref.off()
      this.ref = null
    }
    this.setState({ asyncNode: null })
    if (_.isString(node)) {
      this.ref = this.props.firebase.ref(`node/${node}`)
      this.ref.on('value', n => {
        let asyncNode = _.extend({ id: node }, n.val())
        this.setState(_.extend(this.state, { asyncNode }))
      })
    } else if (_.isFunction((node))) {
      node().then(n => this.setState({ asyncNode: n }))
    }
  }

  componentWillUnmount () {
    if (this.ref) {
      this.ref.off()
    }
  }

  // use this to get this instance's node
  getNode = () =>
    (this.state && this.state.asyncNode) ||
    (this.props && _.isObject(this.props.node) && this.props.node)

  render () {
    const node = this.getNode()
    if (!node) return <p>Node loading</p>
    return (
      <div className={classes.container}>
        <UserDisplay user={node.by} />
        <h1>{node.key}</h1>
        <h2><Link to={`/node/${node.id}`}>{node.value || 'no value'}</Link></h2>
        <p><em>{node.at ? humanize.relativeTime(new Date(node.at).getTime() / 1000) : 'no at'}</em></p>
        {this.props.children}
      </div>
    )
  }
}
