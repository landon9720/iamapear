import React, { Component, PropTypes } from 'react'
import { firebaseConnect, pathToJS, dataToJS } from 'react-redux-firebase'
import classes from './NodeQueries.scss'
import { connect } from 'react-redux'
import _ from 'lodash'
import FlatButton from 'material-ui/FlatButton'
import FontIcon from 'material-ui/FontIcon'
import Node from 'containers/Node/Node'
import TextField from 'material-ui/TextField'
import { browserHistory } from 'react-router'

@firebaseConnect(({ nodeId }) => [{
  path: `node/${nodeId}`,
  storeAs: 'NodeQueries.this'
}, {
  path: 'edge',
  queryParams: ['orderByChild=fromNode', `equalTo=${nodeId}`],
  storeAs: 'outgoingEdges'
}, {
  path: 'edge',
  queryParams: ['orderByChild=toNode', `equalTo=${nodeId}`],
  storeAs: 'incomingEdges'
}])
@connect(({ firebase }) => ({
  auth: pathToJS(firebase, 'auth'),
  node: dataToJS(firebase, 'NodeQueries.this'),
  outgoingEdges: dataToJS(firebase, 'outgoingEdges'),
  incomingEdges: dataToJS(firebase, 'incomingEdges')
}))
export default class NodeQueries extends Component {
  static propTypes = {
    auth: PropTypes.object,
    firebase: PropTypes.object,
    nodeId: PropTypes.string.isRequired,
    node: PropTypes.object,
    outgoingEdges: PropTypes.object,
    incomingEdges: PropTypes.object
  }

  componentWillReceiveProps (nextProps) {
    const { outgoingEdges, incomingEdges } = nextProps
    if (!_.isEqual(this.props.outgoingEdges, outgoingEdges) || !_.isEqual(this.props.incomingEdges, incomingEdges)) {
      const incomingAnswerEdges = _.filter(incomingEdges, e => e.type === 'answer')
      const askingUsers = _.uniq(_.flatMap(outgoingEdges, e => e.type === 'ask' ? [e.toUser] : []))
      const answeringUsers = _.uniq(_.map(incomingAnswerEdges, e => e.fromUser))
      const answeringNodes = _.map(_.filter(incomingAnswerEdges, e => _.includes(askingUsers, e.fromUser)), e => e.fromNode)
      const unaskedAnswers = _.map(_.filter(incomingAnswerEdges, e => !_.includes(askingUsers, e.fromUser)), e => e.fromNode)
      const unansweredAsks = _.flatMap(askingUsers, u => _.includes(answeringUsers, u) ? [] : [u]) // username as key, which is wrong
      this.setState({ answeringNodes, unaskedAnswers, unansweredAsks })
    }
    const thisNodeKey = nextProps.node && nextProps.node.key
    if (thisNodeKey !== (this.props.node && this.props.node.key)) {
      if (this.myContextNodeQuery) {
        this.myContextNodeQuery.off()
        this.myContextNodeQuery = null
        this.setState({ myContextNodeId: null })
      }
      if (nextProps.auth && nextProps.node) {
        const userIdNodeKeyComposite = nextProps.auth.uid + nextProps.node.key
        this.myContextNodeQuery = nextProps.firebase
          .ref('node')
          .orderByChild('userIdNodeKeyComposite')
          .equalTo(userIdNodeKeyComposite)
        this.myContextNodeQuery.on('value', n => {
          n = n.val()
          console.log('myContextNode', n)
          if (n === null) {
            const myContextNodeId = () => {
              return this.props.firebase.push('node', {
                by: this.props.auth.uid,
                key: nextProps.node.key,
                at: new Date().toISOString(),
                userIdNodeKeyComposite: userIdNodeKeyComposite
              }, e => console.log('myContextNodeQuery push callback', e))
              .then(snapshot => console.log('myContextNodeQuery then', snapshot))
            }
            this.setState({ myContextNodeId })
          } else {
            const myContextNodeId = n
            this.setState({ myContextNodeId })
          }
        })
      }
    }
  }

  componentWillUnmount () {
    if (this.myContextNodeQuery) {
      this.myContextNodeQuery.off()
    }
  }

  render () {
    const { nodeId } = this.props || {}
    const { answeringNodes, unaskedAnswers, unansweredAsks, myContextNodeId } = this.state || {}
    return (
      <div className={classes.container}>
        <h3>say</h3>
        {this.say(nodeId)}
        <h3>this.props.nodeId</h3>
        <Node node={nodeId}>{[this.set(nodeId)]}</Node>
        <h3>this.state.myContextNodeId</h3>
        {myContextNodeId ? <Node node={this.state.myContextNodeId} /> : <p>no context node</p>}
        <h3>Answered Asks</h3>
        {answeringNodes ? this.renderNodeList(answeringNodes, n => [this.unask(n), this.set(n), this.delet(n)]) : <p>no answering asks</p>}
        <h3>Unaswered Asks</h3>
        {unansweredAsks ? this.renderNodeList(unansweredAsks, n => [this.unask(n), this.set(n), this.delet(n)]) : <p>no unanswering asks</p>}
        <h3>Unasked Answers</h3>
        {unaskedAnswers ? this.renderNodeList(unaskedAnswers, n => [this.ask(n), this.set(n), this.delet(n)]) : <p>no unaskedAnswers</p>}
      </div>
    )
  }

  renderNodeList = (nodes, nodeChildrenFactory) =>
    nodes.map(n => <Node node={n} key={n}>{nodeChildrenFactory(n)}</Node>)

  say = () => { // no parameter
    if (!this.props.auth) return
    let keyInputTextField
    const handler = () => {
      this.props.firebase.push('node', {
        by: this.props.auth.uid,
        key: keyInputTextField.getValue(),
        at: new Date().toISOString(),
        userIdNodeKeyComposite: this.props.auth.uid + keyInputTextField.getValue()
      }, e => console.log('onSay push callback', e))
      .then(snapshot => browserHistory.push(`/node/${snapshot.key}`))
    }
    return <div>
      <TextField
        floatingLabelText='Key Input'
        ref={t => keyInputTextField = t} />
      <FlatButton
        label='Say'
        onTouchTap={handler}
        icon={<FontIcon className='material-icons'>vpn_key</FontIcon>} />
    </div>
  }

  ask = (n, u) => { // context node asks parameter n user u
    if (!this.props.auth) return
    const mycontextNodeId = this.state.mycontextNodeId
    if (!mycontextNodeId) return
    const handler = () => {
      const promisedFromNodeId = _.isFunction()
        ? mycontextNodeId()
        : Promise.resolve(mycontextNodeId)
      return promisedFromNodeId.then(fromNode => {
        return this.props.firebase.push('edge', {
          type: 'ask',
          fromUser: this.props.auth.uid,
          fromNode,
          toUser: u,
          at: new Date().toISOString()
        }, e => console.trace('ask/push callback', e))
        .then(snapshot => console.log(`/edge/${snapshot.key}`))
      })
    }
    return <FlatButton
      label='Ask'
      onTouchTap={handler}
      key='ask'
      icon={<FontIcon className='material-icons'>add_circle</FontIcon>}
    />
  }

  answer = () => null // TODO

  unask = n => {
    if (!this.props.auth) return
    const edgeId = n
    if (!edgeId) return
    const handler = () =>
      this.props.firebase.ref('edge').child(edgeId).remove(e => console.trace('unask/remove callback', e)).then(a => console.log('unask then', a))
    return <FlatButton
      label='Unask'
      onTouchTap={handler}
      key='unask'
      icon={<FontIcon className='material-icons'>backspace</FontIcon>}
    />
  }

  set = n => {
    if (!this.props.auth) return
    let valueInputTextField
    const handler = () => {
      this.props.firebase.ref(`node/${n}`).set({ value: valueInputTextField.getValue(), at: new Date().toISOString() }).then(a => console.log('set then', a))
    }
    return <div style={{display: 'inline-block'}} key='set'>
      <TextField
        floatingLabelText='Value Input'
        ref={v => valueInputTextField = v} />
      <FlatButton
        label='Set'
        onTouchTap={handler}
        icon={<FontIcon className='material-icons'>message</FontIcon>} />
    </div>
  }

  delet = n => {
    if (!this.props.auth) return
    const handler = () =>
      this.props.firebase.ref(`node/${n}`).set({ value: null, at: new Date().toISOString() }).then(a => console.log('delet then', a))
    return <FlatButton
      label='Delet'
      onTouchTap={handler}
      icon={<FontIcon className='material-icons'>cancel</FontIcon>}
      key='delet'
    />
  }
}
