import React, { Component, PropTypes } from 'react'
import classes from './Trucks.scss'

export default class Trucks extends Component {
  static propTypes = {
    k: PropTypes.string.isRequired // FYI "key" is not a valid prop key
  }

  render () {
    return (
      <div className={classes.container}>
        <h2>Key: {this.props.k}</h2>
      </div>
    )
  }
}
