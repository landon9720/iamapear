import React, { Component, PropTypes } from 'react'
import classes from './NodeList.scss'
import Node from 'containers/Node/Node'
import _ from 'lodash'
import Paper from 'material-ui/Paper'

export default class NodeList extends Component {

  static propTypes = {
    nodes: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.object),
      PropTypes.arrayOf(PropTypes.string)
    ]),
    contextNode: PropTypes.object,
    isAnsweredAsks: PropTypes.bool,
    isUnansweredAsks: PropTypes.bool,
    isUnaskedAnswers: PropTypes.bool,
    isUnrelatedSharedKeys: PropTypes.bool
  }

  render () {
    let { nodes, contextNode, isAnsweredAsks, isUnansweredAsks, isUnaskedAnswers, isUnrelatedSharedKeys } = this.props
    return (
      <Paper className={classes.container} zDepth={4}>
        {nodes.map(n => {
          let k = n.id || _.isString(n) ? n : n.by
          return <Node node={n} key={k} contextNode={contextNode} isAnsweredAsk={isAnsweredAsks} isUnansweredAsk={isUnansweredAsks} isUnaskedAnswer={isUnaskedAnswers} isUnrelatedSharedKey={isUnrelatedSharedKeys} />
        })}
      </Paper>
    )
  }
}
