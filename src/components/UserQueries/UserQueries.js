import React, { Component, PropTypes } from 'react'
import { firebaseConnect, pathToJS, dataToJS, isLoaded } from 'react-redux-firebase'
import classes from './UserQueries.scss'
import NodeList from 'components/NodeList/NodeList.js'
import { connect } from 'react-redux'
import _ from 'lodash'

@firebaseConnect(({ userId }) => [{
  path: 'edge',
  queryParams: ['orderByChild=fromUser', `equalTo=${userId}`],
  storeAs: 'outgoingEdges'
}, {
  path: 'edge',
  queryParams: ['orderByChild=toUser', `equalTo=${userId}`],
  storeAs: 'incomingEdges'
}])
@connect(({ firebase }) => ({
  auth: pathToJS(firebase, 'auth'),
  outgoingEdges: dataToJS(firebase, 'outgoingEdges'),
  incomingEdges: dataToJS(firebase, 'incomingEdges')
}))
export default class UserQueries extends Component {

  static propTypes = {
    auth: PropTypes.object,
    userId: PropTypes.string,
    outgoingEdges: PropTypes.object,
    incomingEdges: PropTypes.object
  }

  componentWillReceiveProps (nextProps) {
    let { outgoingEdges, incomingEdges } = nextProps
    let asks = _.filter(outgoingEdges, e => e.type === 'ask')
    let answers = _.filter(incomingEdges, e => e.type === 'answer')
    let askedAnswers = _.map(_.filter(answers, answer => _.find(asks, ask => ask.fromNode === answer.toNode && ask.toUser === answer.fromUser)), ask => ask.fromNode)
    let unaskedAnswers = _.map(_.filter(answers, answer => !_.find(asks, ask => ask.fromNode === answer.toNode && ask.toUser === answer.fromUser)), ask => ask.fromNode)
    let unansweredAsks = _.map(_.filter(asks, ask => !_.includes(answers, { toNode: ask.fromNode, fromUser: ask.toUser })), ask => ask.fromNode)
    this.setState({ askedAnswers, unaskedAnswers, unansweredAsks })
  }

  render () {
    if (!this.state) { return <p>Still loading 0</p> }

    let { userId, outgoingEdges, incomingEdges } = this.props
    let { askedAnswers, unaskedAnswers, unansweredAsks } = this.state

    if (!isLoaded(userId, outgoingEdges, incomingEdges, askedAnswers, unaskedAnswers, unansweredAsks)) {
      return <p>Still loading 1</p>
    }

    return (
      <div className={classes.container}>
        <h3>Answered Asks</h3>
        <NodeList nodes={askedAnswers} contextUserId={userId} />
        <h3>Unaswered Asks</h3>
        <NodeList nodes={unansweredAsks} contextUserId={userId} />
        <h3>Unasked Answers</h3>
        <NodeList nodes={unaskedAnswers} contextUserId={userId} />
        <h3>Unrelated/Shared Key</h3>
        <NodeList nodes={[]} contextUserId={userId} />
      </div>
    )
  }
}
