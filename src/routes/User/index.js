export default (store) => ({
  path: '/user',
  getChildRoutes (partialNextState, cb) {
    require.ensure([], (require) => {
      const Node = require('./routes/User').default
      cb(null, [
        Node(store)
      ])
    })
  }
})
