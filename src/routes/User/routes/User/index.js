import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, dataToJS, populatedDataToJS } from 'react-redux-firebase'
import _ from 'lodash'
import Node from 'containers/Node/Node'
import Key from 'components/Trucks/Trucks'
import UserQueries from 'components/UserQueries/UserQueries'
import UserDisplay from 'containers/UserDisplay/UserDisplay'

@firebaseConnect(({ params }) => ([
  { path: `user/${params.id}`, storeAs: 'this' }
]))
@connect(
  ({ firebase }, { params }) => ({
    userId: params.id,
    user: dataToJS(firebase, 'this')
  })
)
class UserPage extends Component {

  static propTypes = {
    userId: PropTypes.string.isRequired,
    user: PropTypes.object
  }

  render () {
    const { userId, user } = this.props
    return <div>
      <h1>User Page</h1>
      <UserDisplay user={userId} />
      <UserQueries userId={userId} />
    </div>
  }
  
}

export default (store) => ({
  path: ':id',
  getComponent: (nextState, cb) => cb(null, UserPage)
})
