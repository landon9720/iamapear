import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, dataToJS } from 'react-redux-firebase'
import Key from 'components/Trucks/Trucks'
import NodeQueries from 'components/NodeQueries/NodeQueries'
import nodepage from './nodepage.scss'

@firebaseConnect(({ params }) => ([
  { path: `node/${params.id}`, storeAs: 'NodePage.this' }
]))
@connect(
  ({ firebase }, { params }) => ({
    nodeId: params.id,
    node: dataToJS(firebase, 'NodePage.this')
  })
)
class NodePage extends Component {

  static propTypes = {
    nodeId: PropTypes.string.isRequired,
    node: PropTypes.object
  }

  render () {
    const { nodeId, node } = this.props
    return (
      <div>
        {/*<h1>NodePage nodeId={nodeId}</h1>*/}
        {/*<pre>NodePage this.props {JSON.stringify(this.props, 0, 2)}</pre>*/}
        {/*<Key k={node && node.key ?  node.key : 'loading node page'} />*/}
        <NodeQueries nodeId={nodeId} />
      </div>
    )
  }
}

export default (store) => ({
  path: ':id',
  getComponent: (nextState, cb) => cb(null, NodePage)
})
