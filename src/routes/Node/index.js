// import { LIST_PATH as path } from 'constants'

export default (store) => ({
  path: '/node',
  /*  Async getComponent is only invoked when route matches   */
  // getComponent (nextState, cb) {
  //   /*  Webpack - use 'require.ensure' to create a split point
  //       and embed an async module loader (jsonp) when bundling   */
  //   require.ensure([], (require) => {
  //     /*  Webpack - use require callback to define
  //         dependencies for bundling   */
  //     // const Node = require('./routes/Node').default

  //     /*  Return getComponent   */
  //     cb(null) //Node

  //   /* Webpack named bundle   */
  //   }, 'Node')
  // },
  getChildRoutes (partialNextState, cb) {
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Node = require('./routes/Node').default

      /*  Return getComponent   */
      cb(null, [
        Node(store)
      ])
    })
  }
})
