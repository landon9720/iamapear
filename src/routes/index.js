import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Home from './Home'
import LoginRoute from './Login'
import SignupRoute from './Signup'
import ProjectsRoute from './Projects'
import AccountRoute from './Account'
import NodeRoute from './Node'
import UserRoute from './User'

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { firebaseConnect, pathToJS, dataToJS } from 'react-redux-firebase'

import _ from 'lodash'

import Node from 'containers/Node/Node'

export const createRoutes = (store) => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Home,
  childRoutes: [
    AccountRoute(store),
    LoginRoute(store),
    SignupRoute(store),
    ProjectsRoute(store),
    NodeRoute(store),
    UserRoute(store),
    AllRoute(store)
  ]
})

@firebaseConnect(({ params }) => ([
  {
    path: 'node',
    storeAs: 'nodes'
  }
]))
@connect(
  ({ firebase }, { params }) => ({
    auth: pathToJS(firebase, 'auth'),
    nodes: dataToJS(firebase, 'nodes')
  })
)
class AllPage extends Component {
  static propTypes = {
    auth: PropTypes.object,
    nodes: PropTypes.object
  }

  render () {
    const { nodes } = this.props
    if (!nodes || _.isEmpty(nodes)) {
      return <div>no nodes yet</div>
    }
    return <div>
      {_.map(nodes, (node, nodeId) => <Node key={nodeId} node={_.extend({}, nodes[nodeId], { id: nodeId })} />)}
    </div>
  }
}

const AllRoute = store => ({
  path: '/all',
  getComponent: (nextState, cb) => cb(null, AllPage)
})

export default createRoutes
